/* rmistify.c */

#include "rmistify.h"
#include <raylib.h>
#include <stdlib.h>
#include <stdint.h>
#include <float.h>
#include <limits.h>
#include <string.h>

/* Constraints */
#define MIN_VERTS 3                              // Minimum number of vertices per polygon allowed (3 for a triangle)
#define MIN_POLYS 1                              // Minimum number of polygons allowed (1 to force positives)
#define MIN_CANVAS_W 8                           // Minimum figure canvas width
#define MAX_CANVAS_W INT_MAX                     // Maximum figure canvas width
#define MIN_CANVAS_H 8                           // Minimum figure canvas height
#define MAX_CANVAS_H INT_MAX                     // Maximum figure canvas height
#define DIVISOR_FOR_MOV_MIN 4                    // The maximum absolute value for P_MOV is the screen size divided by this number (for each axis)
#define MIN_SEPARATION (-FLT_MAX / 4.0f)         // Minimum separation between each polygon allowed
#define MAX_SEPARATION (FLT_MAX / 4.0f)          // Maximum separation between each polygon allowed
#define THICKNESS_LIMIT_LOW (-FLT_MAX / 2.0f)    // Lower thickness value allowed
#define THICKNESS_LIMIT_HIGH (FLT_MAX / 2.0f)    // Higher thickness value allowed
#define THICK_DELTA_LIMIT_LOW 0.0f               // Lower thickness variation step value allowed (positive to force positives)
#define THICK_DELTA_LIMIT_HIGH (FLT_MAX / 2.0f)  // Higher thickness variation step value allowed
#define MIN_SPEED (-FLT_MAX / 2.0f)              // Lower speed value allowed
#define MAX_SPEED (FLT_MAX / 2.0f)               // Higher speed value


struct rmt_vertex {
	Vector2 pos;  // Position vector
	Vector2 mov;  // Movement vector
};

struct rmt_figure {
	Rectangle canvas;
	float speed;

	struct {
		size_t num_verts;
		size_t num_polys;
		int v_mov_x_min;
		int v_mov_x_max;
		int v_mov_y_min;
		int v_mov_y_max;
		float separation;
		float dft_speed;
		float dft_thickness;
		float min_thickness;
		float max_thickness;
		float dft_thick_delta;
		float min_thick_delta;
		float max_thick_delta;
		Color dft_color;
	} poly_const;

	struct {
		float steps;
		float initial;
		enum rmt_alpha_modes mode;
	} alpha;

	struct {
		float ness;
		float control_delta;
		float steps;
		float initial;
		enum rmt_thick_modes mode;
		bool automode;
		enum {TD_UP, TD_DOWN} automode_direction;
	} thick;

	struct {
		Color current;
		bool cycling;
		int cycling_phase;
	} color;

	size_t total_verts;
	struct rmt_vertex *verts;
};


static int get_margin(const struct rmt_figure *fig);
static void set_amode_values(struct rmt_figure *figure);
static void set_tmode_values(struct rmt_figure *figure);
static float out_wall(float obj_pos, float wall_pos);


int rmt_validate_args(const struct rmt_args *args)
{
	int error_code = 1;
	size_t num_verts;
	size_t num_polys;
	size_t total_verts;
	Rectangle canvas;

	if (args->numverts < MIN_VERTS) {
		TraceLog(LOG_WARNING, "Argument error: Not enough vertices");
		return error_code;
	}
	++error_code;

	if (args->numpolys < MIN_POLYS) {
		TraceLog(LOG_WARNING,
		         "Argument error: Negative number of trailing polygons");
		return error_code;
	}
	++error_code;

	num_verts = args->numverts;  // Types get cast to size_t
	num_polys = args->numpolys;

	if (num_polys == 0) {
		total_verts = num_verts;
	}
	else {
		total_verts = num_verts * num_polys;
	}

	if (total_verts < num_verts || total_verts < num_polys)	{
		TraceLog(LOG_WARNING,
		         "Argument error: Total quantity of vertices value overflows");
		return error_code;
	}
	++error_code;

	if (total_verts >= (SIZE_MAX / sizeof(struct rmt_vertex)) / 2) {
		TraceLog(LOG_WARNING,
		         "Argument error: Total quantity of vertices is too high");
		return error_code;
	}
	++error_code;

	if (args->canvas_w < MIN_CANVAS_W || args->canvas_w > MAX_CANVAS_W) {
		TraceLog(LOG_WARNING, "Argument error: invalid canvas width");
		return error_code;
	}
	++error_code;

	if (args->canvas_h < MIN_CANVAS_H || args->canvas_h > MAX_CANVAS_H) {
		TraceLog(LOG_WARNING, "Argument error: invalid canvas height");
		return error_code;
	}
	++error_code;

	canvas.width = args->canvas_w;
	canvas.height = args->canvas_h;

	if (args->vxmovmin > args->vxmovmax || args->vymovmin > args->vymovmax) {
		TraceLog(LOG_WARNING,
		         "Argument error: Minimum movement vector higher than maximum");
		return error_code;
	}
	++error_code;

	if (args->vxmovmin < INT_MIN || args->vxmovmin > INT_MAX ||
	    args->vxmovmax < INT_MIN || args->vxmovmax > INT_MAX ||
	    args->vymovmin < INT_MIN || args->vymovmin > INT_MAX ||
	    args->vymovmax < INT_MIN || args->vymovmax > INT_MAX )
	{
		TraceLog(LOG_WARNING,
		         "Argument error: Movement vector way out of range");
		return error_code;
	}
	++error_code;

	if (abs(args->vxmovmin) > canvas.width / DIVISOR_FOR_MOV_MIN ||
	    abs(args->vxmovmax) > canvas.width / DIVISOR_FOR_MOV_MIN ||
	    abs(args->vymovmin) > canvas.height / DIVISOR_FOR_MOV_MIN ||
	    abs(args->vymovmax) > canvas.height / DIVISOR_FOR_MOV_MIN)
	{
		TraceLog(LOG_WARNING,
		         "Argument error: Magnitude of movement vector is too high");
		return error_code;
	}
	++error_code;

	if (args->separatn < MIN_SEPARATION || args->separatn > MAX_SEPARATION) {
		TraceLog(LOG_WARNING, "Argument error: Separation is too high");
		return error_code;
	}
	++error_code;

	if (args->thickmin > args->thickmax) {
		TraceLog(LOG_WARNING,
		         "Argument error: Minimum thickness greater than maximum");
		return error_code;
	}
	++error_code;

	if (args->thickmin < THICKNESS_LIMIT_LOW ||
	    args->thickmax < THICKNESS_LIMIT_LOW ||
	    args->thickmin > THICKNESS_LIMIT_HIGH ||
	    args->thickmax > THICKNESS_LIMIT_HIGH)
	{
		TraceLog(LOG_WARNING, "Argument error: Thicknes range off limits");
		return error_code;
	}
	++error_code;

	if (args->thickdft < args->thickmin || args->thickdft > args->thickmax) {
		TraceLog(LOG_WARNING,
		         "Argument error: Magnitude of default thickness too high");
		return error_code;
	}
	++error_code;

	if (args->thdelmin > args->thdelmax) {
		TraceLog(LOG_WARNING,
		         "Argument error: Minimum thickness delta higher than maximum");
		return error_code;
	}
	++error_code;

	if (args->thdelmin < THICK_DELTA_LIMIT_LOW ||
	    args->thdelmax < THICK_DELTA_LIMIT_LOW ||
	    args->thdelmin > THICK_DELTA_LIMIT_HIGH ||
	    args->thdelmax > THICK_DELTA_LIMIT_HIGH)
	{
		TraceLog(LOG_WARNING,
		         "Argument error: Thickness delta range off limits");
		return error_code;
	}
	++error_code;

	if (args->thdeldft < args->thdelmin || args->thdeldft > args->thdelmax) {
		TraceLog(LOG_WARNING, "Argument error: Thickness delta out of range");
		return error_code;
	}
	++error_code;

	if (args->speeddft < MIN_SPEED || args->speeddft > MAX_SPEED) {
		TraceLog(LOG_WARNING, "Argument error: Speed value out of range");
		return error_code;
	}
	++error_code;

	if (args->alphmode < 0 || args->alphmode >= RMT_NUM_ALPHA_MODES) {
		TraceLog(LOG_WARNING, "Argument error: Alpha mode is not valid");
		return error_code;
	}
	++error_code;

	if (args->thicmode < 0 || args->thicmode >= RMT_NUM_THICK_MODES) {
		TraceLog(LOG_WARNING, "Argument error: Thick mode is not valid");
		return error_code;
	}
	++error_code;

	if (args->color__r < 0 || args->color__g < 0 || args->color__b < 0) {
		TraceLog(LOG_WARNING,
		         "Argument error: Negative color components not valid");
		return error_code;
	}

	if (args->color__r > 255 || args->color__g > 255 || args->color__b > 255) {
		TraceLog(LOG_WARNING, "Color components above limit, expect any color");
	}

	return 0;
}

struct rmt_figure *rmt_create(const struct rmt_args *args)
{
	struct rmt_figure fig;

	if (rmt_validate_args(args) != 0) {
		return NULL;
	}

	fig.canvas.x = 0.0f;
	fig.canvas.y = 0.0f;
	fig.canvas.width = args->canvas_w;
	fig.canvas.height = args->canvas_h;
	fig.poly_const.num_verts = args->numverts;
	fig.poly_const.num_polys = args->numpolys;
	fig.total_verts = fig.poly_const.num_verts * fig.poly_const.num_polys;
	fig.poly_const.separation = args->separatn;
	fig.poly_const.v_mov_x_min = args->vxmovmin;
	fig.poly_const.v_mov_x_max = args->vxmovmax;
	fig.poly_const.v_mov_y_min = args->vymovmin;
	fig.poly_const.v_mov_y_max = args->vymovmax;
	fig.poly_const.dft_speed = args->speeddft;
	fig.poly_const.dft_thickness = args->thickdft;
	fig.poly_const.min_thickness = args->thickmin;
	fig.poly_const.max_thickness = args->thickmax;
	fig.poly_const.dft_thick_delta = args->thdeldft;
	fig.poly_const.min_thick_delta = args->thdelmin;
	fig.poly_const.max_thick_delta = args->thdelmax;
	fig.alpha.mode = args->alphmode;
	fig.thick.mode = args->thicmode;
	fig.thick.ness = fig.poly_const.dft_thickness;
	fig.thick.control_delta = fig.poly_const.dft_thick_delta;
	fig.poly_const.dft_color = (Color){.r = args->color__r % 256,
	                                   .g = args->color__g % 256,
	                                   .b = args->color__b % 256, .a = 255};
	if (args->clorcycl == 0) {
		fig.color.cycling = false;
		fig.color.current = fig.poly_const.dft_color;
	} else {
		fig.color.cycling = true;
		fig.color.current = (Color){.r = 255, .g = 0, .b = 0, .a = 255};
	}
	fig.color.cycling_phase = 0;
	if (args->thickaut == 0) {
		fig.thick.automode = false;
	} else {
		fig.thick.automode = true;
	}
	fig.thick.automode_direction = TD_UP;
	fig.speed = fig.poly_const.dft_speed;
	fig.verts = NULL;

	set_amode_values(&fig);
	set_tmode_values(&fig);

	// Create figure object and vertices array

	struct rmt_figure *figure_object = malloc(sizeof(*figure_object));

	if (figure_object == NULL) {
		TraceLog(LOG_WARNING, "Could not create figure object");
		return NULL;
	}

	memcpy(figure_object, &fig, sizeof(fig));

	struct rmt_vertex *verts = malloc(sizeof(*verts) * fig.total_verts);

	if (verts == NULL) {
		TraceLog(LOG_WARNING, "Could not create vertices");
		free(figure_object);
		return NULL;
	}

	figure_object->verts = verts;

	// Set initial position and movement vectors

	int margin = get_margin(&fig);

	if (margin >= fig.canvas.width / 2 || margin >= fig.canvas.height / 2) {
		TraceLog(LOG_INFO, "Vertices may spawn offscreen");
	}

	for (size_t i = 0; i < fig.poly_const.num_verts; ++i) {
		verts[i].pos.x = GetRandomValue(margin, fig.canvas.width - margin);
		verts[i].pos.y = GetRandomValue(margin, fig.canvas.height - margin);
		verts[i].mov.x = GetRandomValue(fig.poly_const.v_mov_x_min,
		                                fig.poly_const.v_mov_x_max);
		verts[i].mov.y = GetRandomValue(fig.poly_const.v_mov_y_min,
		                                fig.poly_const.v_mov_y_max);
	}

	// Set vertices for the trailing polygons

	for (size_t i = 1; i < fig.poly_const.num_polys; ++i) {
		for (size_t j = 0; j < fig.poly_const.num_verts; ++j) {

			struct rmt_vertex *cur_vert = &verts[i *
			                                     fig.poly_const.num_verts +
			                                     j];
			struct rmt_vertex *pre_vert = &verts[(i - 1) *
			                                     fig.poly_const.num_verts +
			                                     j];

			cur_vert->pos.x = pre_vert->pos.x - (pre_vert->mov.x) *
			                  fig.poly_const.separation;
			cur_vert->pos.y = pre_vert->pos.y - (pre_vert->mov.y) *
			                  fig.poly_const.separation;
			cur_vert->mov.x = pre_vert->mov.x;
			cur_vert->mov.y = pre_vert->mov.y;
		}
	}

	return figure_object;
}

static int get_margin(const struct rmt_figure *fig)
{
	int pmovs[4] = {fig->poly_const.v_mov_x_min, fig->poly_const.v_mov_x_max,
	                fig->poly_const.v_mov_y_min, fig->poly_const.v_mov_y_max};
	int highest_p_mov;

	for (int i = 0; i < 4; ++i) {
		pmovs[i] = abs(pmovs[i]);
	}

	highest_p_mov = pmovs[0];
	for (int i = 1; i < 4; ++i) {
		if (pmovs[i] > highest_p_mov) {
			highest_p_mov = pmovs[i];
		}
	}

	return highest_p_mov * fig->poly_const.separation * fig->poly_const.num_polys;
}

static void set_amode_values(struct rmt_figure *figure)
{
	switch (figure->alpha.mode) {
	case RMT_ALPHA_MODE_NO:
		figure->alpha.steps = 0.0f;
		figure->alpha.initial = 1.0f;
		break;

	case RMT_ALPHA_MODE_FRONT:
		figure->alpha.steps = 1.0f / figure->poly_const.num_polys;
		figure->alpha.initial = figure->alpha.steps;
		break;

	case RMT_ALPHA_MODE_BACK:
		figure->alpha.steps = -1.0f / figure->poly_const.num_polys;
		figure->alpha.initial = 1.0f;
		break;

	default:
		break;
	}
}

static void set_tmode_values(struct rmt_figure *figure)
{
	switch (figure->thick.mode) {
	case RMT_THICK_MODE_CONST:
		figure->thick.steps = 0.0f;
		figure->thick.initial = figure->thick.ness;
		break;

	case RMT_THICK_MODE_FRONT:
		figure->thick.steps = figure->thick.ness / figure->poly_const.num_polys;
		figure->thick.initial = figure->thick.steps;
		break;

	case RMT_THICK_MODE_BACK:
		figure->thick.steps = -figure->thick.ness / figure->poly_const.num_polys;
		figure->thick.initial = figure->thick.ness;
		break;

	default:
		break;
	}
}

void *rmt_destroy(struct rmt_figure *figure)
{
	if (figure != NULL) {
		free(figure->verts);
	}

	free(figure);

	return NULL;
}

void rmt_update(struct rmt_figure *figure)
{
	rmt_update_position(figure);
	rmt_update_thickness(figure);
	rmt_update_colorcycle(figure);
}

void rmt_update_position(struct rmt_figure *figure)
{
	size_t total_verts = figure->total_verts;
	float speed = figure->speed;
	struct rmt_vertex *cur_vert;

	for (size_t i = 0; i < total_verts; ++i) {
		cur_vert = &(figure->verts[i]);

		cur_vert->pos.x += cur_vert->mov.x * speed;
		cur_vert->pos.y += cur_vert->mov.y * speed;
	}

	for (size_t i = 0; i < total_verts; ++i) {
		cur_vert = &(figure->verts[i]);

		if (cur_vert->pos.x < 0.0f) {
			cur_vert->mov.x = -cur_vert->mov.x;
			cur_vert->pos.x = out_wall(cur_vert->pos.x, 0.0f);
		}
		else
		if (cur_vert->pos.x > figure->canvas.width) {
			cur_vert->mov.x = -cur_vert->mov.x;
			cur_vert->pos.x = out_wall(cur_vert->pos.x, figure->canvas.width);
		}

		if (cur_vert->pos.y < 0.0f) {
			cur_vert->mov.y = -cur_vert->mov.y;
			cur_vert->pos.y = out_wall(cur_vert->pos.y, 0.0f);
		}
		else
		if (cur_vert->pos.y > figure->canvas.height) {
			cur_vert->mov.y = -cur_vert->mov.y;
			cur_vert->pos.y = out_wall(cur_vert->pos.y, figure->canvas.height);
		}
	}
}

void rmt_update_thickness(struct rmt_figure *figure)
{
	if (figure->thick.automode == true) {
		if (figure->thick.automode_direction == TD_UP) {
			figure->thick.ness += figure->thick.control_delta;
			if (figure->thick.ness >= figure->poly_const.max_thickness) {
				figure->thick.ness = figure->poly_const.max_thickness;
				figure->thick.automode_direction = TD_DOWN;
			}
		}
		else {  // figure->thick.automode_direction == TD_DOWN
			figure->thick.ness -= figure->thick.control_delta;
			if (figure->thick.ness <= figure->poly_const.min_thickness) {
				figure->thick.ness = figure->poly_const.min_thickness;
				figure->thick.automode_direction = TD_UP;
			}
		}
		set_tmode_values(figure);
	}
}

#if (COLOR_CYCLING_COMPONENT_CLAMP) < 1 || (COLOR_CYCLING_COMPONENT_CLAMP) > 255
 #error COLOR_CYCLING_COMPONENT_CLAMP macro must be in the 0-255 range
#endif
#define COLOR_CYCLING_NUM_PHASES 3
void rmt_update_colorcycle(struct rmt_figure *figure)
{
	if (figure->color.cycling == true) {
		unsigned char *decreasing;
		unsigned char *increasing;

		switch (figure->color.cycling_phase) {
		case 0:
			decreasing = &(figure->color.current.r);
			increasing = &(figure->color.current.g);
			break;
		case 1:
			decreasing = &(figure->color.current.g);
			increasing = &(figure->color.current.b);
			break;
		case 2:
			decreasing = &(figure->color.current.b);
			increasing = &(figure->color.current.r);
			break;
		default:
			decreasing = &(figure->color.current.r);
			increasing = &(figure->color.current.g);
			break;
		}

		--*decreasing;
		++*increasing;

		if (*increasing >= (COLOR_CYCLING_COMPONENT_CLAMP)) {
			*increasing = (COLOR_CYCLING_COMPONENT_CLAMP);
			figure->color.cycling_phase = (figure->color.cycling_phase + 1) %
			                               COLOR_CYCLING_NUM_PHASES;
		}
	}
}

static float out_wall(float obj_pos, float wall_pos)
{
	return 2.0f * wall_pos - obj_pos;
}

void rmt_render(const struct rmt_figure *figure)
{
	float alphaval = figure->alpha.initial;
	float thickval = figure->thick.initial;
	float alpha_steps = figure->alpha.steps;
	float thick_steps = figure->thick.steps;
	size_t num_polys = figure->poly_const.num_polys;
	size_t num_verts = figure->poly_const.num_verts;
	Color color = figure->color.current;

	for (size_t i = num_polys; i > 0; --i) {
		for (size_t j = 0; j < num_verts; ++j) {

			int cur_idx = i * num_verts - num_verts + j;
			int nxt_idx = i * num_verts - num_verts + (j + 1) % num_verts;

			color.a = (unsigned char) (alphaval * 255.0f);
			DrawLineEx(figure->verts[cur_idx].pos, figure->verts[nxt_idx].pos, thickval, color);
			DrawCircleV(figure->verts[cur_idx].pos, thickval / 2.25f, color);
		}

		alphaval += alpha_steps;
		thickval += thick_steps;
	}

}

enum rmt_alpha_modes rmt_get_alpha_mode(const struct rmt_figure *figure)
{
	return figure->alpha.mode;
}

enum rmt_alpha_modes rmt_set_alpha_mode(struct rmt_figure *figure,
                                        enum rmt_alpha_modes amode)
{
	figure->alpha.mode = amode % RMT_NUM_ALPHA_MODES;
	set_amode_values(figure);

	return figure->alpha.mode;
}

float rmt_get_speed(const struct rmt_figure *figure)
{
	return figure->speed;
}

float rmt_set_speed(struct rmt_figure *figure, float speed)
{
	if (speed <= MIN_SPEED || speed >= MAX_SPEED) {
		TraceLog(LOG_WARNING, TextFormat("%s: requested speed value is off-limits", __func__));
		return figure->speed;
	}

	return figure->speed = speed;
}

float rmt_change_speed(struct rmt_figure *figure, float spd_delta)
{
	if (spd_delta <= MIN_SPEED || spd_delta >= MAX_SPEED) {
		TraceLog(LOG_WARNING, TextFormat("%s: magnitude of spd_delta is too high", __func__));
		return figure->speed;
	}

	figure->speed += spd_delta;

	if (figure->speed < MIN_SPEED) {
		figure->speed = MIN_SPEED;
	}
	else
	if (figure->speed > MAX_SPEED) {
		figure->speed = MAX_SPEED;
	}

	return figure->speed;
}

float rmt_set_default_speed(struct rmt_figure *figure)
{
	return figure->speed = figure->poly_const.dft_speed;
}

Color rmt_get_color(const struct rmt_figure *figure)
{
	return figure->color.current;
}

Color rmt_set_color(struct rmt_figure *figure, Color color)
{
	return figure->color.current = color;
}

Color rmt_set_default_color(struct rmt_figure *figure)
{
	return figure->color.current = figure->poly_const.dft_color;
}

bool rmt_colorcycle_active(const struct rmt_figure *figure)
{
	return figure->color.cycling;
}

bool rmt_toggle_colorcycle(struct rmt_figure *figure)
{
	figure->color.cycling = 1 - figure->color.cycling;

	if (figure->color.cycling == true) {
		figure->color.current = (Color){.r = 255, .g = 0, .b = 0, .a = 255};
		figure->color.cycling_phase = 0;
	}
	else {
		figure->color.current = figure->poly_const.dft_color;
	}

	return figure->color.cycling;
}

enum rmt_thick_modes rmt_get_thick_mode(const struct rmt_figure *figure)
{
	return figure->thick.mode;
}

enum rmt_thick_modes rmt_set_thick_mode(struct rmt_figure *figure,
                                        enum rmt_thick_modes tmode)
{
	figure->thick.mode = tmode % RMT_NUM_THICK_MODES;
	set_tmode_values(figure);

	return figure->thick.mode;
}

bool rmt_autothick_active(const struct rmt_figure *figure)
{
	return figure->thick.automode;
}

bool rmt_toggle_autothick(struct rmt_figure *figure)
{
	return figure->thick.automode = 1 - figure->thick.automode;
}

float rmt_get_thick_control_delta(const struct rmt_figure *figure)
{
	return figure->thick.control_delta;
}

float rmt_change_thick_control_delta(struct rmt_figure *figure, float tcd_delta)
{
	figure->thick.control_delta += tcd_delta;

	if (figure->thick.control_delta < figure->poly_const.min_thick_delta) {
		figure->thick.control_delta = figure->poly_const.min_thick_delta;
	}
	else
	if (figure->thick.control_delta > figure->poly_const.max_thick_delta) {
		figure->thick.control_delta = figure->poly_const.max_thick_delta;
	}

	return figure->thick.control_delta;
}

float rmt_set_default_thick_control_delta(struct rmt_figure *figure)
{
	return figure->thick.control_delta = figure->poly_const.dft_thick_delta;
}

float rmt_get_thickness(const struct rmt_figure *figure)
{
	return figure->thick.ness;
}

float rmt_set_thickness(struct rmt_figure *figure, float thickness)
{
	if (thickness < figure->poly_const.min_thickness ||
	    thickness > figure->poly_const.max_thickness)
	{
		TraceLog(LOG_WARNING, TextFormat("%s: Requested thickness is outside valid range", __func__));
		return figure->thick.ness;
	}

	return figure->thick.ness = thickness;
}

float rmt_thickness_up(struct rmt_figure *figure)
{
	figure->thick.ness += figure->thick.control_delta;

	if (figure->thick.ness > figure->poly_const.max_thickness) {
		figure->thick.ness = figure->poly_const.max_thickness;
	}
	set_tmode_values(figure);

	return figure->thick.ness;
}

float rmt_thickness_down(struct rmt_figure *figure)
{
	figure->thick.ness -= figure->thick.control_delta;

	if (figure->thick.ness < figure->poly_const.min_thickness) {
		figure->thick.ness = figure->poly_const.min_thickness;
	}
	set_tmode_values(figure);

	return figure->thick.ness;
}

float rmt_set_default_thickness(struct rmt_figure *figure)
{
	figure->thick.ness = figure->poly_const.dft_thickness;
	set_tmode_values(figure);

	return figure->thick.ness;
}